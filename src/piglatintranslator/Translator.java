package piglatintranslator;

public class Translator {

	public static final String NIL="nil";
	private String phrase;
	
	public Translator(String inputPhrase) {
		this.phrase=inputPhrase;
	}

	public String getPhrase() {
		return this.phrase;
	}

	public String translate() {
		// More words
		if(phrase.contains(" ") || phrase.contains("-") || phrase.contains(".") || phrase.contains(",") || phrase.contains(";")
				|| phrase.contains(":") || phrase.contains("?") || phrase.contains("!")
				|| phrase.contains("'") || phrase.contains("(") || phrase.contains(")")) {
			String[] words = phrase.split("[\\s\\-\\.,;:\\?\\!'\\(\\)]+");
			String output = new String(phrase);
			for(String word : words) {
				output = output.replaceAll(word,translateWord(word));
			}
			return output;
		}
		
		// Single word
		return translateWord(phrase);
	}
	
	private String translateWord(String word) {
		
		if(startsWithVowel() && (word.endsWith("y") || word.endsWith("Y"))) {
			if(isUpperCase(word)) {
				return word + "NAY";
			}
			if(isTitleCase(word)) {
				word = word +"nay";
				word = word.toLowerCase();
				
				String first = word.substring(0,1);
				first = first.toUpperCase();
				String remaining = word.substring(1);
				word = first + remaining;
				
				return word;
			}
			return word + "nay";
		}
		
		if(startsWithVowel() && endsWithVowel()) {
			if(isUpperCase(word)) {
				return word + "YAY";
			}
			if(isTitleCase(word)) {
				word = word +"yay";
				word = word.toLowerCase();
				
				String first = word.substring(0,1);
				first = first.toUpperCase();
				String remaining = word.substring(1);
				word = first + remaining;
				
				return word;
			}
			return word + "yay";
		}
		
		if(startsWithVowel() && !endsWithVowel()) {
			if(isUpperCase(word)) {
				return word + "AY";
			}
			if(isTitleCase(word)) {
				word = word +"ay";
				word = word.toLowerCase();
				
				String first = word.substring(0,1);
				first = first.toUpperCase();
				String remaining = word.substring(1);
				word = first + remaining;
				
				return word;
			}
			return word + "ay";
		}
		
		int numberOfConsonants = numberOfStartingConsonants(word);
		if((numberOfConsonants != 0) && !word.isEmpty()) {
			if(isUpperCase(word)) {
				String consonants = word.substring(0,numberOfConsonants);
				word = word.substring(numberOfConsonants);
				return word + consonants + "AY";
			}
			if(isTitleCase(word)) {
				String consonants = word.substring(0,numberOfConsonants);
				word = word.substring(numberOfConsonants);
				word = word + consonants +"ay";
				word = word.toLowerCase();
				
				String first = word.substring(0,1);
				first = first.toUpperCase();
				String remaining = word.substring(1);
				word = first + remaining;
				
				return word;
			}
			String consonants = word.substring(0,numberOfConsonants);
			word = word.substring(numberOfConsonants);
			return word + consonants + "ay";
		}
		return NIL;
	}
	
	private boolean startsWithVowel() {
		return (phrase.startsWith("a")
		   || phrase.startsWith("e")
		   || phrase.startsWith("i")
		   || phrase.startsWith("o")
		   || phrase.startsWith("u")
		   || phrase.startsWith("A")
		   || phrase.startsWith("E")
		   || phrase.startsWith("I")
		   || phrase.startsWith("O")
		   || phrase.startsWith("U"));
	}
	
	private boolean endsWithVowel() {
		return (phrase.endsWith("a")
		   || phrase.endsWith("e")
		   || phrase.endsWith("i")
		   || phrase.endsWith("o")
		   || phrase.endsWith("u")
		   || phrase.endsWith("A")
		   || phrase.endsWith("E")
		   || phrase.endsWith("I")			  
		   || phrase.endsWith("O")
		   || phrase.endsWith("U"));
	}
	
	private boolean isConsonant(char c) {
		return !(c=='a' || c=='e' || c=='i' || c=='o' || c=='u'
				|| c=='A' || c=='E' || c=='I' || c=='O' || c=='U');
	}
	
	private boolean isTitleCase(String word) {
		Character first = word.charAt(0);
		if(Character.isUpperCase(first)) {
			for(int i=1;i<word.length();i++) {
				if(Character.isUpperCase(word.charAt(i)))
					return false; //Es. biRd
			}
			return true;
		}
		return false;
	}
	
	private boolean isUpperCase(String word) {
		for(int i=0;i<word.length();i++) {
			if(Character.isLowerCase(word.charAt(i)))
				return false;
		}
		return true;
	}
	
	private int numberOfStartingConsonants(String word) {
		int count =0;
		int index =0;
		while(index != word.length()) {
			if(isConsonant(word.charAt(index))) {
				count++;
				index++;
			}
			else {
				return count;
			}
		}
		return count;
	}
}
