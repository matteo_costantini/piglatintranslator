package piglatintranslator;

import static org.junit.Assert.*;
import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhase() {
		String inputPhrase = "Hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("Hello world",translator.getPhrase());
	}
	
	@Test
	public void testNullPhrase() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL,translator.translate());
	}
	
	@Test
	public void testTranslatePhraseStartingWithAEndingWithY() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay",translator.translate());
	}
	
	@Test
	public void testTranslatePhraseStartingWithUEndingWithY() {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay",translator.translate());
	}
	
	@Test
	public void testTranslatePhraseStartingWithVowelEndingWithVowel() {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay",translator.translate());
	}
	
	@Test
	public void testTranslatePhraseStartingWithVowelEndingWithConsonant() {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay",translator.translate());
	}
	
	@Test
	public void testTranslatePhraseStartingWithConsonant() {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay",translator.translate());
	}
	
	@Test
	public void testTranslatePhraseStartingWithMoreConsonants() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay",translator.translate());
	}
	
	@Test
	public void testTranslatePhraseContainingMoreWordsSeparatedBySpaces() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway",translator.translate());
	}
	
	@Test
	public void testTranslatePhraseContainingMoreWordsSeparatedByDashes() {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay",translator.translate());
	}
	
	@Test
	public void testTranslatePhraseContainingMoreWordsContainingPunctuations() {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!",translator.translate());
	}
	
	@Test
	public void testTranslatePhraseContainingMoreWordsContainingConsecutivePunctuations() {
		String inputPhrase = "well-being!!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay!!",translator.translate());
	}
	
	@Test
	public void testTranslatePhraseContainingUpperCaseWords() {
		String inputPhrase = "APPLE";
		Translator translator = new Translator(inputPhrase);
		assertEquals("APPLEYAY",translator.translate());
	}
	
	@Test
	public void testTranslatePhraseContainingTitleCaseWords() {
		String inputPhrase = "Hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("Ellohay",translator.translate());
	}
}
